defmodule BooksStoreApp.PermissionsTest do
  use BooksStoreApp.DataCase

  alias BooksStoreApp.Permissions

  describe "methods" do
    alias BooksStoreApp.Permissions.Methods

    @valid_attrs %{function: "some function", method: "some method"}
    @update_attrs %{function: "some updated function", method: "some updated method"}
    @invalid_attrs %{function: nil, method: nil}

    def methods_fixture(attrs \\ %{}) do
      {:ok, methods} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Permissions.create_methods()

      methods
    end

    test "list_methods/0 returns all methods" do
      methods = methods_fixture()
      assert Permissions.list_methods() |> Kernel.length == 5
    end

    test "get_methods!/1 returns the methods with given id" do
      methods = methods_fixture()
      assert Permissions.get_methods!(methods.uuid) == methods
    end

    test "create_methods/1 with valid data creates a methods" do
      assert {:ok, %Methods{} = methods} = Permissions.create_methods(@valid_attrs)
      assert methods.function == "some function"
      assert methods.method == "some method"
    end

    test "create_methods/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Permissions.create_methods(@invalid_attrs)
    end

    test "update_methods/2 with valid data updates the methods" do
      methods = methods_fixture()
      assert {:ok, %Methods{} = methods} = Permissions.update_methods(methods, @update_attrs)
      assert methods.function == "some updated function"
      assert methods.method == "some updated method"
    end

    test "update_methods/2 with invalid data returns error changeset" do
      methods = methods_fixture()
      assert {:error, %Ecto.Changeset{}} = Permissions.update_methods(methods, @invalid_attrs)
      assert methods == Permissions.get_methods!(methods.uuid)
    end

    test "delete_methods/1 deletes the methods" do
      methods = methods_fixture()
      assert {:ok, %Methods{}} = Permissions.delete_methods(methods)
      assert_raise Ecto.NoResultsError, fn -> Permissions.get_methods!(methods.uuid) end
    end

    test "change_methods/1 returns a methods changeset" do
      methods = methods_fixture()
      assert %Ecto.Changeset{} = Permissions.change_methods(methods)
    end
  end
end
