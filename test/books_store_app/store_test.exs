defmodule BooksStoreApp.BookTest do
  use BooksStoreApp.DataCase

  alias BooksStoreApp.Book
  alias BooksStoreApp.Store.Authors
  alias BooksStoreApp.Store.Books

  @book_valid_attrs %{description: "some description", name: "some name"}
  @book_update_attrs %{description: "some updated description", name: "some updated name"}
  @book_invalid_attrs %{description: nil, name: nil}

  @valid_attrs %{firstname: "some firstname"}
  @update_attrs %{firstname: "some updated firstname"}
  @invalid_attrs %{firstname: nil}

  def authors_fixture(attrs \\ %{}) do
    {:ok, authors} =
      attrs
      |> Enum.into(@valid_attrs)
      |> Book.create_authors()

    authors
  end

  def books_fixture(attrs \\ %{}) do
    {:ok, books} =
      attrs
      |> Enum.into(@book_valid_attrs)
      |> Book.create_books()

    books
  end

  describe "books" do
    test "list_books/0 returns all books" do
      books = books_fixture()
      assert Book.list_books() == [books]
    end

    test "get_books!/1 returns the books with given id" do
      books = books_fixture()
      assert Book.get_books!(books.uuid) == books
    end

    test "create_books/1 with valid data creates a books" do
      assert {:ok, %Books{} = books} = Book.create_books(@book_valid_attrs)
      assert books.description == "some description"
      assert books.name == "some name"
    end

    test "create_books/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Book.create_books(@book_invalid_attrs)
    end

    test "update_books/2 with valid data updates the books" do
      books = books_fixture()
      assert {:ok, %Books{} = books} = Book.update_books(books, @book_update_attrs)
      assert books.description == "some updated description"
      assert books.name == "some updated name"
    end

    test "update_books/2 with invalid data returns error changeset" do
      books = books_fixture()
      assert {:error, %Ecto.Changeset{}} = Book.update_books(books, @book_invalid_attrs)
      assert books == Book.get_books!(books.uuid)
    end

    test "delete_books/1 deletes the books" do
      books = books_fixture()
      assert {:ok, %Books{}} = Book.delete_books(books)
      assert_raise Ecto.NoResultsError, fn -> Book.get_books!(books.uuid) end
    end

    test "change_books/1 returns a books changeset" do
      books = books_fixture()
      assert %Ecto.Changeset{} = Book.change_books(books)
    end
  end

  describe "authors" do
    test "list_authors/0 returns all authors" do
      authors = authors_fixture()
      assert Book.list_authors() == [authors]
    end

    test "get_authors!/1 returns the authors with given id" do
      authors = authors_fixture()
      assert Book.get_authors!(authors.uuid) == authors
    end

    test "create_authors/1 with valid data creates a authors" do
      assert {:ok, %Authors{} = authors} = Book.create_authors(@valid_attrs)
      assert authors.firstname == "some firstname"
    end

    test "create_authors/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Book.create_authors(@invalid_attrs)
    end

    test "update_authors/2 with valid data updates the authors" do
      authors = authors_fixture()
      assert {:ok, %Authors{} = authors} = Book.update_authors(authors, @update_attrs)
      assert authors.firstname == "some updated firstname"
    end

    test "update_authors/2 with invalid data returns error changeset" do
      authors = authors_fixture()
      assert {:error, %Ecto.Changeset{}} = Book.update_authors(authors, @invalid_attrs)
      assert authors == Book.get_authors!(authors.uuid)
    end

    test "delete_authors/1 deletes the authors" do
      authors = authors_fixture()
      assert {:ok, %Authors{}} = Book.delete_authors(authors)
      assert_raise Ecto.NoResultsError, fn -> Book.get_authors!(authors.uuid) end
    end

    test "change_authors/1 returns a authors changeset" do
      authors = authors_fixture()
      assert %Ecto.Changeset{} = Book.change_authors(authors)
    end
  end

  describe "books and author relations" do
    test "given a book update it and add one author" do
      book = books_fixture(@book_valid_attrs)
      author = authors_fixture(@valid_attrs)

      assert %Books{
               authors: [
                 %Authors{
                   firstname: "some firstname"
                 }
               ]
             } = Book.add_author_to_book(book, author.uuid)
    end

    test "given an author update it and add one book" do
      book = books_fixture(@book_valid_attrs)
      author = authors_fixture(@valid_attrs)

      assert %Authors{
               books: [
                 %Books{
                   name: "some name"
                 }
               ]
             } = Book.add_book_to_author(author, book.uuid)
    end
  end
end
