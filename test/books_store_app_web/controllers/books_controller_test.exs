defmodule BooksStoreAppWeb.BooksControllerTest do
  use BooksStoreAppWeb.ConnCase

  alias BooksStoreApp.Book
  alias BooksStoreApp.Store.Books
  alias BooksStoreApp.Auth
  alias BooksStoreApp.Repo
  alias Plug.Test

  @create_attrs %{
    description: "some description",
    name: "some name"
  }
  @update_attrs %{
    description: "some updated description",
    name: "some updated name"
  }
  @invalid_attrs %{
    description: nil,
    name: nil
  }

  def fixture(:books) do
    {:ok, books} = Book.create_books(@create_attrs)
    books
  end

  def fixture(:current_user) do
    {:ok, user} =
      Auth.create_user(%{
        is_active: true,
        password: "some password",
        username: "some username"
      })

    for perm <- [{"create", "POST"}, {"update", "PUT"}, {"delete", "DELETE"}] do
      Auth.add_permission_method(user |> Repo.preload(:methods), %{
        function: perm |> elem(0),
        method: perm |> elem(1)
      })
    end

    user
  end

  setup %{conn: conn} do
    {:ok, conn: conn, current_user: current_user} = setup_current_user(conn)
    {:ok, conn: put_req_header(conn, "accept", "application/json"), current_user: current_user}
  end

  describe "index" do
    test "lists all books", %{conn: conn} do
      conn = get(conn, Routes.books_path(conn, :index))
      assert json_response(conn, 200)["books"] == []
    end
  end

  describe "create books" do
    test "renders books when data is valid", %{conn: conn} do
      conn = post(conn, Routes.books_path(conn, :create), books: @create_attrs)
      assert %{"uuid" => id} = json_response(conn, 201)["book"]

      conn = get(conn, Routes.books_path(conn, :show, id))

      assert %{
               "uuid" => id,
               "authors" => [],
               "description" => "some description",
               "name" => "some name"
             } = json_response(conn, 200)["book"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.books_path(conn, :create), books: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update books" do
    setup [:create_books]

    test "renders books when data is valid", %{conn: conn, books: %Books{uuid: id} = books} do
      conn = put(conn, Routes.books_path(conn, :update, books), books: @update_attrs)
      assert %{"uuid" => ^id} = json_response(conn, 200)["book"]

      conn = get(conn, Routes.books_path(conn, :show, id))

      assert %{
               "uuid" => id,
               "description" => "some updated description",
               "name" => "some updated name",
               "authors" => []
             } = json_response(conn, 200)["book"]
    end

    test "renders errors when data is invalid", %{conn: conn, books: books} do
      conn = put(conn, Routes.books_path(conn, :update, books), books: @invalid_attrs)

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete books" do
    setup [:create_books]

    test "deletes chosen books", %{conn: conn, books: books} do
      conn = delete(conn, Routes.books_path(conn, :delete, books))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.books_path(conn, :show, books))
      end
    end
  end

  defp create_books(_) do
    books = fixture(:books)
    {:ok, books: books}
  end

  defp setup_current_user(conn) do
    current_user = fixture(:current_user)

    {:ok,
     conn: Test.init_test_session(conn, current_user_id: current_user.uuid),
     current_user: current_user}
  end
end
