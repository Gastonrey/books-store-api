defmodule BooksStoreAppWeb.AuthorsControllerTest do
  use BooksStoreAppWeb.ConnCase

  alias BooksStoreApp.Book
  alias BooksStoreApp.Store.Authors
  alias BooksStoreApp.Auth
  alias BooksStoreApp.Repo
  alias Plug.Test

  @create_attrs %{
    firstname: "some firstname",
    lastname: "some lastname"
  }
  @update_attrs %{
    firstname: "some updated firstname",
    lastname: "some updated lastname"
  }
  @invalid_attrs %{firstname: nil, lastname: nil}

  def fixture(:authors) do
    {:ok, authors} = Book.create_authors(@create_attrs)
    authors
  end

  def fixture(:current_user) do
    {:ok, user} =
      Auth.create_user(%{
        is_active: true,
        password: "some password",
        username: "some username"
      })

    for perm <- [{"create", "POST"}, {"update", "PUT"}, {"delete", "DELETE"}] do
      Auth.add_permission_method(user |> Repo.preload(:methods), %{
        function: perm |> elem(0),
        method: perm |> elem(1)
      })
    end

    user
  end

  setup %{conn: conn} do
    {:ok, conn: conn, current_user: current_user} = setup_current_user(conn)
    {:ok, conn: put_req_header(conn, "accept", "application/json"), current_user: current_user}
  end

  describe "index" do
    test "lists all authors", %{conn: conn} do
      conn = get(conn, Routes.authors_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create authors" do
    test "renders authors when data is valid", %{conn: conn} do
      conn = post(conn, Routes.authors_path(conn, :create), authors: @create_attrs)
      assert %{"uuid" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.authors_path(conn, :show, id))

      assert %{
               "uuid" => id,
               "firstname" => "some firstname",
               "lastname" => "some lastname"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.authors_path(conn, :create), authors: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update authors" do
    setup [:create_authors]

    test "renders authors when data is valid", %{
      conn: conn,
      authors: %Authors{uuid: id} = authors
    } do
      conn = put(conn, Routes.authors_path(conn, :update, authors), authors: @update_attrs)
      assert %{"uuid" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.authors_path(conn, :show, id))

      assert %{
               "uuid" => id,
               "firstname" => "some updated firstname",
               "lastname" => "some updated lastname"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, authors: authors} do
      conn = put(conn, Routes.authors_path(conn, :update, authors), authors: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete authors" do
    setup [:create_authors]

    test "deletes chosen authors", %{conn: conn, authors: authors} do
      conn = delete(conn, Routes.authors_path(conn, :delete, authors))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.authors_path(conn, :show, authors))
      end
    end
  end

  defp create_authors(_) do
    authors = fixture(:authors)
    {:ok, authors: authors}
  end

  defp setup_current_user(conn) do
    current_user = fixture(:current_user)

    {:ok,
     conn: Test.init_test_session(conn, current_user_id: current_user.uuid),
     current_user: current_user}
  end
end
