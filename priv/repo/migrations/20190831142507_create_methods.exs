defmodule BooksStoreApp.Repo.Migrations.CreateMethods do
  use Ecto.Migration

  def change do
    create table(:methods) do
      add :function, :string
      add :method, :string
      add :user_uuid, references(:users, type: :binary_id, column: :uuid)

      timestamps()
    end
  end
end
