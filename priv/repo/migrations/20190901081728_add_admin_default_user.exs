defmodule BooksStoreApp.Repo.Migrations.AddAdminDefaultUser do
  use Ecto.Migration

  alias BooksStoreApp.Auth
  alias BooksStoreApp.Repo

  def up do
    {:ok, user} = Auth.create_user(%{is_active: true, password: "123456", username: "admin"})

    for perm <- [{"create", "POST"}, {"update", "PUT"}, {"delete", "DELETE"}, {"update", "PATCH"}] do
      Auth.add_permission_method(user |> Repo.preload(:methods), %{
        function: perm |> elem(0),
        method: perm |> elem(1)
      })
    end
  end
end
