defmodule BooksStoreApp.Repo.Migrations.CreateBooks do
  use Ecto.Migration

  def change do
    create table(:authors) do
      add :firstname, :string
      add :lastname, :string

      timestamps()
    end

    create table(:books) do
      add :name, :string
      add :description, :string

      timestamps()
    end

    create table(:books_authors) do
      add :books_uuid, references(:books, type: :binary_id, column: :uuid)
      add :authors_uuid, references(:authors, type: :binary_id, column: :uuid)
    end

    create(index(:books_authors, [:books_uuid]))
    create(index(:books_authors, [:authors_uuid]))

    create unique_index(:books, [:name])
  end
end
