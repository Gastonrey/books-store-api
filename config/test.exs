use Mix.Config

# Configure your database
config :books_store_app, BooksStoreApp.Repo,
  username: "root",
  password: "",
  database: "books_store_app_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox,
  migration_primary_key: [name: :uuid, type: :binary_id],
  migration_timestamps: [type: :utc_datetime]

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :books_store_app, BooksStoreAppWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :bcrypt_elixir, :log_rounds, 4
