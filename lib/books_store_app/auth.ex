defmodule BooksStoreApp.Auth do
  @moduledoc """
  The Auth context.
  """

  import Ecto.Query, warn: false
  alias BooksStoreApp.Repo

  alias BooksStoreApp.Auth.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id) |> Repo.preload(:methods)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end

  @doc """
  Returns a user if has been authenticated

  ## Examples

      iex> authenticate_user(user)
      {:ok, user}

      or

      iex> authenticate_user(wrong_user_pass)
      {:error, "Wrong username or password"}
  """
  def authenticate_user(username, password) do
    query = from(u in User, where: u.username == ^username)
    query |> Repo.one() |> verify_password(password)
  end

  @doc """
  Returns a user with methods

  ## Examples

      iex> add_permission_method(user, %{function: "foo", method: "GET"})
      {:ok, user}

      or

      iex> add_permission_method(user, %{field: "wrong value"})
      {:error, %Ecto.Changeset{}}
  """
  def add_permission_method(user, attrs) do
    user
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:methods, [attrs | user.methods])
    |> Repo.update()
  end

  def can?(conn, user_uuid, action, method) when is_binary(user_uuid) do
    user = get_user!(user_uuid)

    unless conn.method == "GET" do
      user_method =
        Enum.find(user.methods, fn m ->
          m.method == method
        end)

      user_function =
        Enum.find(user.methods, fn m ->
          m.function == Atom.to_string(action)
        end)

      !is_nil(user_method && user_function)
    end
  end

  defp verify_password(nil, _) do
    # Perform a dummy check to make user enumeration more difficult
    Bcrypt.no_user_verify()
    {:error, "Wrong username or password"}
  end

  defp verify_password(user, password) do
    if Bcrypt.verify_pass(password, user.password_hash) do
      {:ok, user}
    else
      {:error, "Wrong username or password"}
    end
  end
end
