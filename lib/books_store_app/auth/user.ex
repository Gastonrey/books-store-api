defmodule BooksStoreApp.Auth.User do
  use BooksStoreApp.Schema
  import Ecto.Changeset

  schema "users" do
    field :is_active, :boolean, default: false
    field :password, :string, virtual: true
    field :password_hash, :string
    field :username, :string
    has_many :methods, BooksStoreApp.Permissions.Methods

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :is_active, :password])
    |> validate_required([:username, :is_active, :password])
    |> unique_constraint(:username)
    |> put_password_hash()
  end

  defp put_password_hash(
         %Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset
       ) do
    change(changeset, password_hash: Bcrypt.hash_pwd_salt(password))
  end

  defp put_password_hash(changeset) do
    changeset
  end
end
