defmodule BooksStoreApp.Permissions do
  @moduledoc """
  The Permissions context.
  """

  import Ecto.Query, warn: false
  alias BooksStoreApp.Repo

  alias BooksStoreApp.Permissions.Methods
  alias BooksStoreApp.Auth.User

  @doc """
  Returns the list of methods.

  ## Examples

      iex> list_methods()
      [%Methods{}, ...]

  """
  def list_methods do
    Repo.all(Methods)
  end

  @doc """
  Gets a single methods.

  Raises `Ecto.NoResultsError` if the Methods does not exist.

  ## Examples

      iex> get_methods!(123)
      %Methods{}

      iex> get_methods!(456)
      ** (Ecto.NoResultsError)

  """
  def get_methods!(id), do: Repo.get!(Methods, id)

  @doc """
  Creates a methods.

  ## Examples

      iex> create_methods(%{field: value})
      {:ok, %Methods{}}

      iex> create_methods(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_methods(attrs \\ %{}) do
    %Methods{}
    |> Methods.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a methods.

  ## Examples

      iex> update_methods(methods, %{field: new_value})
      {:ok, %Methods{}}

      iex> update_methods(methods, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_methods(%Methods{} = methods, attrs) do
    methods
    |> Methods.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Methods.

  ## Examples

      iex> delete_methods(methods)
      {:ok, %Methods{}}

      iex> delete_methods(methods)
      {:error, %Ecto.Changeset{}}

  """
  def delete_methods(%Methods{} = methods) do
    Repo.delete(methods)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking methods changes.

  ## Examples

      iex> change_methods(methods)
      %Ecto.Changeset{source: %Methods{}}

  """
  def change_methods(%Methods{} = methods) do
    Methods.changeset(methods, %{})
  end
end
