defmodule BooksStoreApp.Repo do
  use Ecto.Repo,
    otp_app: :books_store_app,
    adapter: Ecto.Adapters.MyXQL
end
