defmodule BooksStoreApp.Book do
  @moduledoc """
  The Book context.
  """

  import Ecto.Query, warn: false
  alias BooksStoreApp.Repo

  alias BooksStoreApp.Store.Books
  alias BooksStoreApp.Store.Authors

  @doc """
  Returns the list of books.

  ## Examples

      iex> list_books()
      [%Books{}, ...]

  """
  def list_books do
    Books
    |> Repo.all()
    |> Repo.preload(:authors)
  end

  @doc """
  Gets a single books.

  Raises `Ecto.NoResultsError` if the Books does not exist.

  ## Examples

      iex> get_books!(123)
      %Books{}

      iex> get_books!(456)
      ** (Ecto.NoResultsError)

  """
  def get_books!(id), do: Repo.get!(Books, id) |> Repo.preload(:authors)

  @doc """
  Creates a books.

  ## Examples

      iex> create_books(%{field: value})
      {:ok, %Books{}}

      iex> create_books(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  ## attrs
    * `:name` - the book's name.
    * `:description` - Book's description
    * `:authors` - OPTIONAL It's a list of maps:
      [%{firstname: "foo", lastname: "bar", ...}]

  """
  def create_books(attrs \\ %{}) do
    result =
      %Books{}
      |> Books.changeset(attrs)
      |> Repo.insert()

    case result do
      {:ok, book} ->
        authors = Map.take(attrs, ~w(authors))

        if authors != %{} do
          %{"authors" => authors} = authors

          Enum.each(authors, fn author ->
            {:ok, author} = create_authors(author)

            book_authors_relation(book, author)
          end)
        end

        {:ok, get_books!(book.uuid)}

      {:error, _changeset} = error ->
        error
    end
  end

  @doc """
  Updates a books.

  ## Examples

      iex> update_books(books, %{field: new_value})
      {:ok, %Books{}}

      iex> update_books(books, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  ## attrs
    * `:name` - the book's name.
    * `:description` - Book's description
    * `:authors` - OPTIONAL It's a list of maps:
      [%{firstname: "foo", lastname: "bar", ...}]

      In :authors map if :uuid is passed then author gets assocciated to book

  """
  def update_books(%Books{} = books, attrs) do
    author_uuid = get_in(attrs, ["authors", "uuid"])

    books
    |> add_author_to_book(author_uuid)
    |> Books.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Add Author to a Book

  ## Examples

      iex> add_author_to_book(book, author)
      {:ok, %Books{}}

      iex> add_author_to_book(book, %{field: bad_param})
      {:error, %Ecto.Changeset{}}

  """
  def add_author_to_book(%Books{} = book, author_uuid) do
    with %Authors{} = author <- get_authors!(author_uuid) do
      assoc = book_authors_relation(book, author)
      assoc.book |> Repo.preload(:authors)
    end
  rescue
    _e in ArgumentError -> book
  end

  @doc """
  Deletes a Books.

  ## Examples

      iex> delete_books(books)
      {:ok, %Books{}}

      iex> delete_books(books)
      {:error, %Ecto.Changeset{}}

  """
  def delete_books(%Books{} = books) do
    Repo.delete(books)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking books changes.

  ## Examples

      iex> change_books(books)
      %Ecto.Changeset{source: %Books{}}

  """
  def change_books(%Books{} = books) do
    Books.changeset(books, %{})
  end

  @doc """
  Returns the list of authors.

  ## Examples

      iex> list_authors()
      [%Authors{}, ...]

  """
  def list_authors do
    Authors
    |> Repo.all()
    |> Repo.preload(:books)
  end

  @doc """
  Gets a single authors.

  Raises `Ecto.NoResultsError` if the Authors does not exist.

  ## Examples

      iex> get_authors!(123)
      %Authors{}

      iex> get_authors!(456)
      ** (Ecto.NoResultsError)

  """
  def get_authors!(id), do: Repo.get!(Authors, id) |> Repo.preload(:books)

  @doc """
  Creates a authors.

  ## Examples

      iex> create_authors(%{field: value})
      {:ok, %Authors{}}

      iex> create_authors(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_authors(attrs \\ %{}) do
    result =
      %Authors{}
      |> Authors.changeset(attrs)
      |> Repo.insert()

    case result do
      {:ok, author} ->
        books = Map.take(attrs, ~w(books))

        if books != %{} do
          %{"books" => books} = books

          Enum.each(books, fn book ->
            {:ok, book} = create_books(book)

            book_authors_relation(book, author)
          end)
        end

        {:ok, get_authors!(author.uuid)}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
  Updates a authors.

  ## Examples

      iex> update_authors(authors, %{field: new_value})
      {:ok, %Authors{}}

      iex> update_authors(authors, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_authors(%Authors{} = authors, attrs) do
    book_uuid = get_in(attrs, ["books", "uuid"])

    authors
    |> add_book_to_author(book_uuid)
    |> Authors.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Add Book to author.

  ## Examples

      iex> add_book_to_author(author, book_attrs)
      {:ok, %Authors{}}

      iex> add_book_to_author(author, %{field: bad_param})
      {:error, %Ecto.Changeset{}}

  """
  def add_book_to_author(%Authors{} = author, book_uuid) do
    with %Books{} = book <- get_books!(book_uuid) do
      assoc = book_authors_relation(book, author)
      assoc.author |> Repo.preload(:books)
    end
  rescue
    _e in ArgumentError -> author
  end

  @doc """
  Deletes a Authors.

  ## Examples

      iex> delete_authors(authors)
      {:ok, %Authors{}}

      iex> delete_authors(authors)
      {:error, %Ecto.Changeset{}}

  """
  def delete_authors(%Authors{} = authors) do
    Repo.delete(authors)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking authors changes.

  ## Examples

      iex> change_authors(authors)
      %Ecto.Changeset{source: %Authors{}}

  """
  def change_authors(%Authors{} = authors) do
    Authors.changeset(authors, %{})
  end

  defp book_authors_relation(book, author) do
    %BooksAuthors{books_uuid: book.uuid, authors_uuid: author.uuid}
    |> Repo.insert!()
    |> Repo.preload([:author, :book])
  end
end
