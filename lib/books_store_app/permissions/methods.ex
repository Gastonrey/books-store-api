defmodule BooksStoreApp.Permissions.Methods do
  use BooksStoreApp.Schema
  import Ecto.Changeset

  schema "methods" do
    field :function, :string
    field :method, :string

    belongs_to :users, BooksStoreApp.Auth.User,
      foreign_key: :user_uuid,
      references: :uuid,
      type: :binary_id

    timestamps()
  end

  @doc false
  def changeset(methods, attrs) do
    methods
    |> cast(attrs, [:function, :method])
    |> validate_required([:function, :method])
  end
end
