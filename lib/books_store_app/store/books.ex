defmodule BooksStoreApp.Store.Books do
  use BooksStoreApp.Schema
  import Ecto.Changeset

  schema "books" do
    field :description, :string
    field :name, :string

    has_many :books_authors, BooksAuthors
    has_many :authors, through: [:books_authors, :author]

    timestamps()
  end

  @doc false
  def changeset(books, attrs) do
    books
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
    |> unique_constraint(:name, name: :books_name_index, message: "Duplicated book name")
  end
end
