defmodule BooksAuthors do
  use BooksStoreApp.Schema
  import Ecto.Changeset

  @already_exists "ALREADY_EXISTS"

  schema "books_authors" do
    belongs_to :book, BooksStoreApp.Store.Books,
      foreign_key: :books_uuid,
      references: :uuid,
      type: :binary_id

    belongs_to :author, BooksStoreApp.Store.Authors,
      foreign_key: :authors_uuid,
      references: :uuid,
      type: :binary_id
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> Ecto.Changeset.cast(params, [:book_uuid, :authors_uuid])
    |> Ecto.Changeset.validate_required([:book_uuid, :authors_uuid])
    |> foreign_key_constraint(:book_uuid)
    |> foreign_key_constraint(:authors_uuid)
    |> unique_constraint([:books, :authors],
      name: :book_uuid_authors_uuid_unique_index,
      message: @already_exists
    )

    # Maybe do some counter caching here!
  end
end
