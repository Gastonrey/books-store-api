defmodule BooksStoreApp.Store.Authors do
  use BooksStoreApp.Schema
  import Ecto.Changeset

  schema "authors" do
    field :firstname, :string
    field :lastname, :string

    has_many :books_authors, BooksAuthors
    has_many :books, through: [:books_authors, :book]

    timestamps()
  end

  @doc false
  def changeset(authors, attrs) do
    authors
    |> cast(attrs, [:firstname, :lastname])
    |> validate_required([:firstname])
  end
end
