defmodule BooksStoreApp.Plug.CheckPermissions do
  @moduledoc """
  This plug makes sure that the authenticated user is a authorized for action,
  otherwise it halts the connection.
  """

  import Plug.Conn
  import Phoenix.Controller

  alias BooksStoreAppWeb.ErrorView
  alias BooksStoreApp.Repo
  alias BooksStoreApp.Auth

  def init(opts) do
    Enum.into(opts, %{})
  end

  def call(conn, opts \\ []) do
    check_allowed(conn, opts)
  end

  defp check_allowed(conn, _opts) do
    current_user_uuid = get_session(conn, :current_user_id)

    case Auth.can?(conn, current_user_uuid, conn.private[:phoenix_action], conn.method) do
      false -> halt_plug(conn)
      true -> assign(conn, :allowed, true)
      nil -> assign(conn, :allowed, true)
    end
  end

  defp halt_plug(conn) do
    conn
    |> put_status(403)
    |> put_view(BooksStoreAppWeb.ErrorView)
    |> render("403.json", message: "Action not allowed")
    |> halt()
  end
end
