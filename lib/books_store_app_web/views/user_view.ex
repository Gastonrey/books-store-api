defmodule BooksStoreAppWeb.UserView do
  use BooksStoreAppWeb, :view
  alias BooksStoreAppWeb.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{id: user.uuid, username: user.username, is_active: user.is_active}
  end

  def render("sign_in.json", %{user: user}) do
    %{
      data: %{
        user: %{
          id: user.uuid,
          username: user.username
        }
      }
    }
  end
end
