defmodule BooksStoreAppWeb.BooksView do
  use BooksStoreAppWeb, :view
  alias BooksStoreAppWeb.BooksView

  def render("index.json", %{books: books}) do
    %{books: render_many(books, BooksView, "books.json")}
  end

  def render("show.json", %{books: books}) do
    %{book: render_one(books, BooksView, "books.json")}
  end

  def render("books.json", %{books: books}) do
    %{
      uuid: books.uuid,
      name: books.name,
      description: books.description,
      authors: get_authors_from(books.authors)
    }
  end

  defp get_authors_from(books) do
    Enum.map(books, fn b ->
      b
      |> Map.from_struct()
      |> Map.take([:firstname, :lastname])
    end)
  end
end
