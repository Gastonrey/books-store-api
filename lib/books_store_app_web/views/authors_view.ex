defmodule BooksStoreAppWeb.AuthorsView do
  use BooksStoreAppWeb, :view
  alias BooksStoreAppWeb.AuthorsView

  def render("index.json", %{authors: authors}) do
    %{data: render_many(authors, AuthorsView, "authors.json")}
  end

  def render("show.json", %{authors: authors}) do
    %{data: render_one(authors, AuthorsView, "authors.json")}
  end

  def render("authors.json", %{authors: authors}) do
    %{
      uuid: authors.uuid,
      firstname: authors.firstname,
      lastname: authors.lastname,
      books: get_books_from(authors.books)
    }
  end

  defp get_books_from(authors) do
    Enum.map(authors, fn a ->
      a
      |> Map.from_struct()
      |> Map.take([:name, :description])
    end)
  end
end
