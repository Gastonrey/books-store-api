defmodule BooksStoreAppWeb.Router do
  use BooksStoreAppWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  pipeline :api_auth do
    plug :ensure_authenticated
  end

  scope "/api", BooksStoreAppWeb do
    pipe_through :api
    post "/users/sign_in", UserController, :sign_in
  end

  scope "/api", BooksStoreAppWeb do
    pipe_through [:api, :api_auth]
    resources "/users", UserController, except: [:new, :edit]
    resources "/books", BooksController, except: [:new, :edit]
    resources "/authors", AuthorsController, except: [:new, :edit]
  end

  # Plug function
  defp ensure_authenticated(conn, _opts) do
    current_user_id = get_session(conn, :current_user_id)

    if is_binary(current_user_id) or conn.method == "GET" do
      conn
    else
      conn
      |> put_status(:unauthorized)
      |> put_view(BooksStoreAppWeb.ErrorView)
      |> render("401.json", message: "Unauthenticated user")
      |> halt()
    end
  end
end
