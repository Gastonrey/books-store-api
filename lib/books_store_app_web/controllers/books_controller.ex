defmodule BooksStoreAppWeb.BooksController do
  use BooksStoreAppWeb, :controller

  alias BooksStoreApp.Book
  alias BooksStoreApp.Store.Books

  action_fallback BooksStoreAppWeb.FallbackController
  plug BooksStoreApp.Plug.CheckPermissions

  def index(conn, _params) do
    books = Book.list_books()
    render(conn, "index.json", books: books)
  end

  def create(conn, %{"books" => books_params}) do
    with {:ok, %Books{} = books} <- Book.create_books(books_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.books_path(conn, :show, books))
      |> render("show.json", books: books)
    end
  end

  def show(conn, %{"id" => id}) do
    books = Book.get_books!(id)
    render(conn, "show.json", books: books)
  end

  def update(conn, %{"id" => uuid, "books" => books_params}) do
    books = Book.get_books!(uuid)

    with {:ok, %Books{} = books} <- Book.update_books(books, books_params) do
      render(conn, "show.json", books: books)
    end
  end

  def delete(conn, %{"id" => uuid}) do
    books = Book.get_books!(uuid)

    with {:ok, %Books{}} <- Book.delete_books(books) do
      send_resp(conn, :no_content, "")
    end
  end
end
