defmodule BooksStoreAppWeb.AuthorsController do
  use BooksStoreAppWeb, :controller

  alias BooksStoreApp.Book
  alias BooksStoreApp.Store.Authors

  action_fallback BooksStoreAppWeb.FallbackController

  plug BooksStoreApp.Plug.CheckPermissions

  def index(conn, _params) do
    authors = Book.list_authors()
    render(conn, "index.json", authors: authors)
  end

  def create(conn, %{"authors" => authors_params}) do
    with {:ok, %Authors{} = authors} <- Book.create_authors(authors_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.authors_path(conn, :show, authors))
      |> render("show.json", authors: authors)
    end
  end

  def show(conn, %{"id" => id}) do
    authors = Book.get_authors!(id)
    render(conn, "show.json", authors: authors)
  end

  def update(conn, %{"id" => id, "authors" => authors_params}) do
    authors = Book.get_authors!(id)

    with {:ok, %Authors{} = authors} <- Book.update_authors(authors, authors_params) do
      render(conn, "show.json", authors: authors)
    end
  end

  def delete(conn, %{"id" => id}) do
    authors = Book.get_authors!(id)

    with {:ok, %Authors{}} <- Book.delete_authors(authors) do
      send_resp(conn, :no_content, "")
    end
  end
end
