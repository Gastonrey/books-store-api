## Table of content
  - [Description](#description)
  - [Start Up and Authentication](#start-up-and-authentication)
  - [Permissions](#permissions)

## Description

This project is a REST API that contains the endpoints described in Postman collection which has been attached in `fixtures/` folder.

Use the Phoenix framework to build an API REST to manage (create, edit, delete) Books and Authors database with Ecto, partial authentication using HTTP auth-basic capabilities against the database, and error management.

Database:

 - MySQL

## Start Up and Authentication:

Before starting up the API make sure you setup the database:

`MIX_ENV={your env} mix ecto.setup`

By running this command this will create all the database structure but also will create an initial admin user.
Default credentials for this admin user are:
username: `admin`
password: `123456`

*Make sure you change them to a more secret ones.*

This User is useful since this API has Authentication and permissions validations. See #permissions section bellow.

If all previous steps where ok, just start it up as convenient, i.e: `MIX_ENV={env} mix phx.server` will start on port 4000.

To test the API there is a Postman collection in `fixture/` folder.

**Important:** In order to send request to endpoints you should sign-in first, so look for sign-in endpoint sample, grab your cookie and include this cookie on every request you send, otherwise you will not be able to access.

## Permissions

This API has a username and password sign-in that returns a cookie session in order to be able to request endpoints, this cookie is expected on every request you send by including the `Cookie` header.

Also these API has restrictions for request as poer User, method and function. These means after creating a User make sure to add these permissions. See sample at `fixtures/` folder.

## Docs

This projects uses `ex_docs`, so run `mix docs` if you would like to see html documented modules.
